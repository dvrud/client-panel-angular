export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyAYgfVSOhXw2bn6Zt0AXrm6T5BHmHV3FwQ",
    authDomain: "client-panel-prod-46905.firebaseapp.com",
    databaseURL: "https://client-panel-prod-46905.firebaseio.com",
    projectId: "client-panel-prod-46905",
    storageBucket: "client-panel-prod-46905.appspot.com",
    messagingSenderId: "1080957293830",
    appId: "1:1080957293830:web:c491ad10c421d3b2"
  }
};
