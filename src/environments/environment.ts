// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAYgfVSOhXw2bn6Zt0AXrm6T5BHmHV3FwQ",
    authDomain: "client-panel-prod-46905.firebaseapp.com",
    databaseURL: "https://client-panel-prod-46905.firebaseio.com",
    projectId: "client-panel-prod-46905",
    storageBucket: "client-panel-prod-46905.appspot.com",
    messagingSenderId: "1080957293830",
    appId: "1:1080957293830:web:c491ad10c421d3b2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
